console.log("=== soal No.1 ===")

//Normal javaScript
/*const golden = function goldenFunction(){
    console.log("this is golden!!")
  }
   
  golden()*/
//after ES6
goldenFunction = () => {
    console.log("This is golden!")
}
goldenFunction()

console.log()
console.log("=== soal No.2 ===")
//Before
/*const newFunction = function literal(firstName, lastName){
    return {
      firstName1: firstName,
      lastName1: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName()  */

//After
const newFunction = function literal(firstName, lastName){
    return{ 
    fullName : function() {
        console.log(firstName+ ' '+lastName)
        return
        }
    }
}   
  //Driver Code 
  newFunction("William", "Imoh").fullName()

console.log()
console.log("=== soal No.3 ===")
//before
/*const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

const firstName = newObject.firstName;
const lastName = newObject.lastName;
const destination = newObject.destination;
const occupation = newObject.occupation;

console.log(newObject)*/
//after
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const { firstName, lastName, destination, occupation, spell } = newObject;

console.log(firstName, lastName, destination, occupation)


console.log()
console.log("=== soal No.4 ===")
/*//before
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)
//Driver Code
console.log(combined)*/

//after
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combinedArray = [...west, ...east]
console.log(combinedArray)

console.log()
console.log("=== soal No.5 ===")
//before
/*const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
 
// Driver Code
console.log(before) */

//after
const lorem = 'Lorem glassdolor sit amet'
const consecture = 'consecture adipiscing elit'
const earth = 'earthdo eiusmod tempor incididunt ut labore et dolore magna aliqua.'
const ut = 'Ut enim ad minim veniam' 

const gabung = `${lorem}, ${consecture}, ${earth} ${ut}`

console.log(gabung)
