import React,{} from 'react';
import { View, Text, TextInput, StyleSheet, Image, TouchableOpacity, Platform, Button } from 'react-native';


  const LoginScreen = () => {
    return (
      <View style={styles.container}>
         <Image source={require('./images/logo.png')} style={{marginTop: 63}} />
         <Text style={{fontSize: 25, textAlign: 'center', color: '#003366', marginVertical: 50}}>Login</Text>
         <View style={styles.login}>
           <Text style={styles.text}>Username</Text>
           <TextInput style={styles.input}/>
         </View>
         <View style={styles.login}>
           <Text style={styles.text}  >Password</Text>
           <TextInput style={styles.input} secureTextEntry={true}/>
         </View>

        
        <TouchableOpacity style={styles.btnMasuk}>
           <Text style={styles.btnText}>Masuk</Text>
         </TouchableOpacity>

          <Text style={styles.atau}>Atau</Text>

         <TouchableOpacity style={styles.btnDaftar}>
           <Text style={styles.btnText}>Daftar?</Text>
         </TouchableOpacity>
        
      </View>
    )
  }

  export default LoginScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  text: {
    color: '#003366',
    fontSize: 20
  },
  login:{
    marginHorizontal: 30,
    marginVertical: 5,
    alignContent: 'center'
  },
  input: {
    padding: 5,
    borderColor: '#003366',
    borderWidth: 1,
    height: 40
  },
  btnLogin: {
    backgroundColor: '#090909',
  },
  textMasuk: {
    fontSize: 24,
    color: '#fff00f'
  },
  atau: {
    color: '#3ec6ff',
    fontSize: 24,
    alignSelf: 'center',
  },
  btnMasuk: {
    alignSelf: 'center',
    backgroundColor: '#3ec6ff',
    alignItems: 'center',
    padding: 10,
    marginTop: 20,
    marginHorizontal: 30,
    marginBottom: 10,
    borderRadius: 15,
    width: 140
  },
  btnText: {
    color: '#ffffff',
    fontSize: 24,
  },
  btnDaftar: {
    alignSelf: 'center',
    backgroundColor: '#003366',
    alignItems: 'center',
    padding: 10,
    marginTop: 20,
    marginHorizontal: 30,
    marginBottom: 10,
    borderRadius: 15,
    width: 140
  }
  
});
