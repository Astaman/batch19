import React, {}from 'react';
import { View, Text, Line, TextInput, StyleSheet, Image, TouchableOpacity, Platform, Button, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const AboutScreen = () => {
    return (
        <ScrollView>
            <View style={styles.container}>
                <Text style={styles.tentangSaya}>Tentang Saya</Text>
                    <Icon style={styles.iconUser} name="user-circle" size={100} />
                <Text style={styles.nama}>Ade Kostaman</Text>
                <Text style={styles.pekerjaan}>IOS Develover</Text>
                    <View style={styles.boxPortofolio}>
                        <Text style={styles.portofolio}>Portofolio</Text>
                        <View style={styles.kotak}>
                            <View style={styles.iconPortofolio}>
                                <Icon name="gitlab" size={40} color={'#3ec6ff'} />
                                <Text>@Astaman</Text>
                            </View>
                            <View style={styles.iconPortofolio}>
                                <Icon name="github" size={40} color={'#3ec6ff'} />
                                <Text>@Astaman</Text>
                            </View>
                        </View>
                    </View>

                    <View style={styles.boxContact}>
                        <Text style={styles.hubungi}>Hubungi Saya</Text>
                        <View style={styles.kotakDalam1}>
                            <View style={styles.kotakDalam2}>
                                <View>
                                    <Icon name="facebook" size={40} style={styles.Icon}/>
                                    <Text textAlign={'center'}>@Astaman</Text>
                                </View>
                            </View>
                            <View style={styles.kotakDalam2}>
                                <View>
                                    <Icon name="twitter" size={40} style={styles.Icon} />
                                    <Text>@Astaman</Text>
                                </View>
                            </View>
                            <View style={styles.kotakDalam2}>
                                <View>
                                    <Icon name="instagram" size={40} style={styles.Icon} />
                                    <Text>@Astaman</Text>
                                </View>
                            </View>
                        </View>

                    </View>

            </View>    
        </ScrollView>
    )
}

export default AboutScreen

const styles = StyleSheet.create({
    container: {
        marginTop: 64
    },
    tentangSaya: {
        fontSize: 36,
        color: '#003366',
        textAlign: 'center'
    },
    iconUser: {
        alignSelf: 'center',
        marginVertical: 10
    },
    nama: {
        textAlign: 'center',
        fontSize: 24,
        color: '#003366'
    },
    pekerjaan: {
        textAlign: 'center',
        fontSize: 16,
        color: '#3ec6ff'
    },
    boxPortofolio: {
        borderRadius: 15,
        alignSelf: 'center',
        marginVertical: 16,
        width: 359,
        height: 140,
        backgroundColor: '#efefef'
    },
    kotak: {
        width: 359,
        borderColor: '#003366',
        borderTopWidth: 2,
        flexDirection: 'row',
        justifyContent: 'space-around',
        
    },
    portofolio: {
        padding: 5,
        fontSize: 18,
        color: '#003366'
    },
    iconPortofolio: {
        marginTop: 10,
        
    },
    boxContact: {
        borderRadius: 15,
        alignSelf: 'center',
        marginVertical: 16,
        width: 359,
        height: 251,
        backgroundColor: '#efefef'
    },
    hubungi: {
        padding: 5,
        fontSize: 18,
        color: '#003366'
    },
    kotakDalam1: {
        borderTopWidth: 2,
        width: 359,
        borderColor: '#003366',
    },
    kotakDalam2: {
        textAlign: 'center',
        color: '#003366'
    },
    Icon: {
        marginTop: 10,
        color: '#3ec6ff',
        alignSelf: 'center'
    }
})