import { createStore } from 'redux'
import { reducer } from './todoListRedux.js'

// Mendefinisikan store menggunakan reducer
const store = createStore(reducer)

export default store