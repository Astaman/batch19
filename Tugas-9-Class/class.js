console.log("=== Soal Nomor 1. Animal Class ===")
class Animal {
    constructor(name){
        this.animalName = name;
        this.animalLegs = 4;
        this.animalCold = false;
    }
    get animal() {
        return this.animalName; this.animalLegs; this.animalCold

    }
    set animal(name) {
        this.animalName=name;
        this.legs = this.animalLegs;
        
    }
}

sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.animal) // 4
console.log(sheep.animal) // false

console.log("=== sola No. 2 ===")
function Clock({ template }) {
  
    var timer;
  
    function render() {
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    this.stop = function() {
      clearInterval(timer);
    };
  
    this.start = function() {
      render();
      timer = setInterval(render, 1000);
    };
  
  }
  
  var clock = new Clock({template: 'h:m:s'});
  clock.start(); 
 
  class Clock {
    constructor(template)
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  

//Kalo saya setintervalnya jadi function arrow
this.timer = setInterval(()=>this.render(), 1000);

function Clock({ template }) {
  
    var timer;
  
    function render() {
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    this.stop = function() {
      clearInterval(timer);
    };
  
    this.start = function() {
      render();
      timer = setInterval(render, 1000);
    };
  
  }
  
  var clock = new Clock({template: 'h:m:s'});
  clock.start(); 
 
  class Clock {
    // Code di sini
}
