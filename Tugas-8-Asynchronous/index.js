// di index.js
let readBooks = require('./callback.js')
 
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
function baca(time, book, i) {
    if (i < books.length) {
        readBooks(time,books[i], function (sisa) {
            if (sisa >0 ) {
                i++;
                baca(sisa, books, i);
            }
        })
    }
}

/*books.forEach(element => readBooks(10000, element, (callbackFn) => {
    console.log(callbackFn)
  }))*/

baca(9000, books, 0)
